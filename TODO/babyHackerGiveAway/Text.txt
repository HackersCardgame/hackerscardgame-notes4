Halli Hallo Matthias

Danke für die Antwort im Kontext Geschichten hinter dem Vignoble.

Wird solcher "Content" dann nur mit Wein verkauft, oder kann man das auch z.B. mit alkoholfreien Traubensaft oder Rimus beziehen. Technisch vermutlich ein "Attribut" (Informatik) auf dem Objekt (Objekt Orientierte Programmierung) anheften. Apropos vor 30 Jahren jeweils am 31.12. im Keller Raum am Ahornweg, da war es ja auch Rimus und kein Alkohol.

Zu Deiner Einladung:
Meine Meinung ist, dass Alkohol zu promoten auch nur mässig toll ist. Ich finde es zwar auch nicht gut Alkohol komplett zu verbieten, aber die Kinder gleich an der Konfirmation in Wein zu "ersäufen" ist sicher noch schlechter.

Ich bin zwar Christ aber so wie es in einigen Quellen zur jüdischen Kultur steht: "Alkohol Ja, ABER nur in geringen Mengen" besser, aber zum Synthesizer Musik machen trinke ich öfters mal ein Bier maximal zwei.. Da wäre auch die Frage, wenn man jetzt so ein Vignoble anschauen gehen würde, denken die auch mal an die Kinder? Letztes Bild.

Sowieso zum letzen Bild: was machen Kinder in der Familie unterdessen? Interessieren sich die Kinder schon dafür? z.B. wir Landolt-Gigax hatten in Bettenhausen einen Bauernhof mit ganz viele Räumen zum Entdecken, das ist vermutlich für ein Kind viel interessanter.

Dann Erkenntnisse von der CyberWAR Front, Psychologie nennt man in Hacker-Sprache "Social Engineering":
Die Kinder heute werden mit dem Handy dazu getrieben nur Benutzer zu sein und nicht etwas selber zu machen. Hacker Ethik sagt aber "hands on!", also selber etwas zu machen mit Elektronik oder Informatik bzw. egal in welchem Fachgebiet.

Psychologische Hürde:
Das Problem ist da vermutlich, dass wenn man den Kindern verbietet die erste 20 Lebensjahre TV schauen zu schauen oder Alkohol zu trinken, dass man das dann mit 20 erst recht tut. Psychologisch würde man dabei von "Kontrasteffekt auf Depriavation" sprechen. Ich z.B. hab mir als Ältester mir immer die Fernbedingung gekrallt weil ich halt als ältester der stärkste bei den 1980er Landolt Kindern war. Somit war ab irgend 20 das TV-Schauen für mich schon abgehakt und ich mich zum Thema Cyber weitergebildet.

Konkrete Frage:
Also mein kleiner Bruder der Dominik hat jetzt einen 3D Drucker bekommen, weshalb sollte man jetzt das Geld für Wein statt z.B. für einen 3D Drucker ausgeben?

PS: als ich das beim ersten mal abschicken wollte wurde ich wieder mal zer-cyber't, apropos "first blood wenn man das entsprechende DEFCON Video findet", bzw. haben die so etwas wie eine Active Forward Censorship infrastruktur wo ich dann zer'cyber't werde und zeltgleich mit eine RAGEMASTER oder eine Active Denial System gegrillt werde. Plausibel dass der Täterkreis aus Küttigen Rain stammen würde und vermutich älter als wir 1980er Generation

MfG von der CyberWar front, müsst Ihr Euch nicht so fest sorgen machen, die greifen >> zur Zeit << vorallem die Menschen an, die irgendwie begabt sind um "Hacking Exposed", 1. Ausgabe 1999 zu zitieren: "The System is insecure to those who know about a security isssue"

PPS: keine Zeit für perfekte Rechtschreibung...



7social engeneering = Psycholoigie




Danke für die Antwort im Kontext der Geschichten hinter dem Vignoble wo man für die Kultur auch noch etwas zahlt. Wird solcher "Content" dann nur mit Wein verkauft, oder kann man das auch z.B. mit alkoholfreien Traubensaft oder Rimus kaufen, apropos vor 30 Jahren jeweils am 31.12. im Keller Raum am Ahornweg?

Zu Deiner Einladung:
Weil meine Meinung ist, dass Ritual Alkohol zu promoten auch nur mässig toll ist. Ich finde es zwar auch nicht gut wie Moslems  Alkohol komplett zu verbieten, aber die Kinder gleich an der Konfirmation in Wein zu "ersäufen" ist sicher noch schlechter. Ich bin zwar Christ aber so wie es in einigen Quellen zur jüdischen Kultur steht: "Alkohol Ja, aber nur in geringen Mengen" besser. Da wäre auch die Frage: wenn man jetzt so ein Vignoble anschauen gehen würde, gibt es da Angebote für Kinder => Letztes Bild.

sowieso zum letzen Bild: was machen Kinder in der Familie unterdessen? Interessieren sich die Kinder schon dafür? z.B. wir Landolt-Gigax hatten in Bettenhausen einen Bauernhof mit ganz viele Räumen zum Entdecken, das ist vermutlich für ein Kind viel toller als z.B. so doofe Kunst im Kunsthaus.

Dann Erkenntnisse von der CyberWAR Front, Psychologie nennt man in HackerSprache "Social Engineering":
Die Kinder heute werden oft auch mit dem Handy dazu getrieben nur Benutzer zu sein und nicht etwas selber zu machen. Hacker Ethik sagt aber "hands on!", also selber etwas machen, egal in welchem Fachgebiet. Mein Bruder Domink hat jetzt zB. einen 3D Drucker. Ein Problem ist, dass wenn man das den Kindern verbietet TV zu schauen, dass wenn man mit 20 auszieht erst recht z.B. TV schauen würde, weil sie nicht durften. Psychologisch würde man dabei von "Kontrasteffekt auf Depriavation" sprechen. Ich z.B. hab als ältester mir immer die Fernbedingung genommen, weil ich halt als Ältester der Stärkste bei den 1980er Landolts war, was für mich dann bedeutet hat, dass ab irgend 20 das TV-Schauen schon abgehakt war und ich mich im Cyber, Wissenschaften, Hacking, Social Engeneering, Elektortechnick... weitergebildet habe.



MfG von der CyberWar Front, PS müsst Ihr Euch nicht so fest sorgen machen, die Amerikaner (bzw einige durchgeknallte Amerikaner weil nicht alle Amis sind durchgeknallt) greifen >> zur Zeit << vorallem die Menschen an, die irgendwie begabt sind; um "Hacking Exposed", 1. Ausgabe 1999 zu zitieren: "The System is insecure to those who know about a security isssue"
